﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class D_QuanLyMuonTra
    {
        string madg, masach, soluong, ngaymuon, ngaytra, ngayhentra;

        public string Ngaymuon
        {
            get { return ngaymuon; }
            set { ngaymuon = value; }
        }

        public string Ngayhentra
        {
            get { return ngayhentra; }
            set { ngayhentra = value; }
        }

      

        public string Ngaytra
        {
            get { return ngaytra; }
            set { ngaytra = value; }
        }

        public string Soluong
        {
            get { return soluong; }
            set { soluong = value; }
        }

        public string Masach
        {
            get { return masach; }
            set { masach = value; }
        }

        public string Madg
        {
            get { return madg; }
            set { madg = value; }
        }
    }
}
