﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class D_QuanLyDocGia
    {
        string madg, tendg, gioitinh, diachi, ngaymuon;

        public string Ngaymuon
        {
            get { return ngaymuon; }
            set { ngaymuon = value; }
        }

        public string Diachi
        {
            get { return diachi; }
            set { diachi = value; }
        }

        public string Gioitinh
        {
            get { return gioitinh; }
            set { gioitinh = value; }
        }

        public string Tendg
        {
            get { return tendg; }
            set { tendg = value; }
        }

        public string Madg
        {
            get { return madg; }
            set { madg = value; }
        }
    }
}
