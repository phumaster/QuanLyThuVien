﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class D_QuanLyTacGia
    {
        string matg, tentg, diachi;

        public string Diachi
        {
            get { return diachi; }
            set { diachi = value; }
        }

        public string Tentg
        {
            get { return tentg; }
            set { tentg = value; }
        }

        public string Matg
        {
            get { return matg; }
            set { matg = value; }
        }
    }
}
