USE [master]
GO
/****** Object:  Database [QLThuVien]    Script Date: 12/28/2016 9:59:37 PM ******/
CREATE DATABASE [QLThuVien]
USE [QLThuVien]
GO
ALTER DATABASE [QLThuVien] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLThuVien].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLThuVien] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLThuVien] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLThuVien] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLThuVien] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLThuVien] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLThuVien] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLThuVien] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLThuVien] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLThuVien] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLThuVien] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLThuVien] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLThuVien] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLThuVien] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLThuVien] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLThuVien] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLThuVien] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLThuVien] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLThuVien] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLThuVien] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLThuVien] SET  MULTI_USER 
GO
ALTER DATABASE [QLThuVien] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLThuVien] SET DB_CHAINING OFF 
GO
USE [QLThuVien]
GO
/****** Object:  Table [dbo].[LoaiSach]    Script Date: 12/28/2016 9:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiSach](
	[MaLoaiSach] [char](10) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
	[KieuSach] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiSach] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MuonTraSach]    Script Date: 12/28/2016 9:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MuonTraSach](
	[MaDG] [char](10) NOT NULL,
	[MaSach] [char](10) NOT NULL,
	[SoLuong] [int] NULL,
	[NgayMuon] [date] NULL,
	[NgayHenTra] [date] NULL,
	[NgayTra] [date] NULL,
 CONSTRAINT [PK_MuonTraSach] PRIMARY KEY CLUSTERED 
(
	[MaDG] ASC,
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NguoiMuon]    Script Date: 12/28/2016 9:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NguoiMuon](
	[MaDG] [char](10) NOT NULL,
	[TenDG] [nvarchar](50) NULL,
	[GioiTinh] [char](10) NULL,
	[NgayMuon] [date] NULL,
	[DiaChi] [nvarchar](50) NULL,
 CONSTRAINT [PK_NguoiMuon] PRIMARY KEY CLUSTERED 
(
	[MaDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sach]    Script Date: 12/28/2016 9:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sach](
	[MaSach] [char](10) NOT NULL,
	[TenSach] [nvarchar](100) NULL,
	[SoLuong] [int] NULL,
	[MaTG] [char](10) NULL,
	[MaLoaiSach] [char](10) NULL,
 CONSTRAINT [PK_Sach] PRIMARY KEY CLUSTERED 
(
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TacGia]    Script Date: 12/28/2016 9:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TacGia](
	[MaTG] [char](10) NOT NULL,
	[TenTG] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](50) NULL,
 CONSTRAINT [PK_TacGia] PRIMARY KEY CLUSTERED 
(
	[MaTG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [TenLoai], [KieuSach]) VALUES (N'SGK       ', N'Sách giáo khoa', N'Sách')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [TenLoai], [KieuSach]) VALUES (N'TC        ', N'Tạp chí', N'Báo')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [TenLoai], [KieuSach]) VALUES (N'TH        ', N'Tin học', N'Sách')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [TenLoai], [KieuSach]) VALUES (N'TT        ', N'Truyện tranh', N'Truyện')
INSERT [dbo].[MuonTraSach] ([MaDG], [MaSach], [SoLuong], [NgayMuon], [NgayHenTra], [NgayTra]) VALUES (N'DG1       ', N'S1        ', 3, CAST(N'2016-09-20' AS Date), CAST(N'2016-09-27' AS Date), CAST(N'2016-09-27' AS Date))
INSERT [dbo].[MuonTraSach] ([MaDG], [MaSach], [SoLuong], [NgayMuon], [NgayHenTra], [NgayTra]) VALUES (N'DG2       ', N'S2        ', 2, CAST(N'2016-04-04' AS Date), CAST(N'2016-04-13' AS Date), CAST(N'2016-04-13' AS Date))
INSERT [dbo].[MuonTraSach] ([MaDG], [MaSach], [SoLuong], [NgayMuon], [NgayHenTra], [NgayTra]) VALUES (N'DG3       ', N'S3        ', 3, CAST(N'2016-09-20' AS Date), CAST(N'2016-09-27' AS Date), CAST(N'2016-09-27' AS Date))
INSERT [dbo].[MuonTraSach] ([MaDG], [MaSach], [SoLuong], [NgayMuon], [NgayHenTra], [NgayTra]) VALUES (N'DG4       ', N'S2        ', 2, CAST(N'2016-04-04' AS Date), CAST(N'2016-04-13' AS Date), CAST(N'2016-04-13' AS Date))
INSERT [dbo].[NguoiMuon] ([MaDG], [TenDG], [GioiTinh], [NgayMuon], [DiaChi]) VALUES (N'DG1       ', N'Nguyên Văn Hải', N'Nam       ', CAST(N'2013-10-10' AS Date), N'Hà Nội')
INSERT [dbo].[NguoiMuon] ([MaDG], [TenDG], [GioiTinh], [NgayMuon], [DiaChi]) VALUES (N'DG2       ', N'Lê Khánh', N'Nu        ', CAST(N'2014-10-10' AS Date), N'Sài Gòn')
INSERT [dbo].[NguoiMuon] ([MaDG], [TenDG], [GioiTinh], [NgayMuon], [DiaChi]) VALUES (N'DG3       ', N'Nguyễn Khánh', N'Nu        ', CAST(N'2016-11-01' AS Date), N'Hà Nam')
INSERT [dbo].[NguoiMuon] ([MaDG], [TenDG], [GioiTinh], [NgayMuon], [DiaChi]) VALUES (N'DG9       ', N'Nguyễn Mai', N'Nam       ', CAST(N'2014-11-10' AS Date), N'Hà Nội')
INSERT [dbo].[Sach] ([MaSach], [TenSach], [SoLuong], [MaTG], [MaLoaiSach]) VALUES (N'S1        ', N'Anh Văn', 100, N'TG1       ', N'SGK       ')
INSERT [dbo].[Sach] ([MaSach], [TenSach], [SoLuong], [MaTG], [MaLoaiSach]) VALUES (N'S2        ', N'Sức Khỏe và đời sống', 120, N'TG2       ', N'TC        ')
INSERT [dbo].[Sach] ([MaSach], [TenSach], [SoLuong], [MaTG], [MaLoaiSach]) VALUES (N'S3        ', N'Vật lí', 300, N'TG3       ', N'SGK       ')
INSERT [dbo].[Sach] ([MaSach], [TenSach], [SoLuong], [MaTG], [MaLoaiSach]) VALUES (N'S5        ', N'Hệ cơ sở dự liệu SQL', 300, N'TG4       ', N'TH        ')
INSERT [dbo].[Sach] ([MaSach], [TenSach], [SoLuong], [MaTG], [MaLoaiSach]) VALUES (N'S6        ', N'Thần đồng đất việt', 250, N'TG3       ', N'TT        ')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [DiaChi]) VALUES (N'TG1       ', N'Từ Thị Xuân Hiền', N'Huế')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [DiaChi]) VALUES (N'TG2       ', N'Hoàng Thị Mai', N'Hà Nội')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [DiaChi]) VALUES (N'TG3       ', N'Trần Văn Khôi', N'Hồ Chí Minh')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [DiaChi]) VALUES (N'TG4       ', N'Lê Thị Thảo', N'Hà Nội')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [DiaChi]) VALUES (N'TG5       ', N'Nguyễn Hoàng Mai', N'Huế')
USE [master]
GO
ALTER DATABASE [QLThuVien] SET  READ_WRITE 
GO
