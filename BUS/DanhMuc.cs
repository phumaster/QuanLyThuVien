﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;

namespace BUS
{
    public class DanhMuc
    {
        Connect_Data cd = new Connect_Data();
        public DataTable HienThi()
        {
            string sql = "select * from Sach";
            DataTable dt = new DataTable();
            dt = cd.getTable(sql);
            return dt;
        }
        public DataTable HienThiDocGia()
        {
            string sql = "select * from NguoiMuon";
            DataTable dt = new DataTable();
            dt = cd.getTable(sql);
            return dt;
        }
        public DataTable HienThiTacGia()
        {
            string sql = "select * from TacGia";
            DataTable dt = new DataTable()
;
            dt = cd.getTable(sql);
            return dt;
        }
        public DataTable HienThiMuonSach()
        {
            string sql = "select MaDG,MaSach,SoLuong,NgayMuon,NgayHenTra from MuonTraSach";
            DataTable dt = new DataTable();
            dt = cd.getTable(sql);
            return dt;
        }
        public DataTable HienThiTraSach()
        {
            string sql = "select *from MuonTraSach";
            DataTable dt = new DataTable();
            dt = cd.getTable(sql);
            return dt;
        }
    }
}
