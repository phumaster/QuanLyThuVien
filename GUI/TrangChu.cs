﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using BUS;
namespace GUI
{
    public partial class TrangChu : Form
    {
        public TrangChu()
        {
            InitializeComponent();
        }

        DanhMuc dm = new DanhMuc();
        Connect_Data cd = new Connect_Data();
        private void TrangChu_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dm.HienThi();
        }

        public void loadData()
        {
            dataGridView1.DataSource = dm.HienThi();
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TrangChu_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show("Bạn có muốn đóng chương trình không?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dl == DialogResult.No)
                e.Cancel = true;
        }

        private void btncapnhat_Click(object sender, EventArgs e)
        {
            CapNhat f = new CapNhat();
            f.ShowDialog();
        }

        private void btnquanlydocgia_Click(object sender, EventArgs e)
        {
            QuanLyDocGia f = new QuanLyDocGia();
            f.ShowDialog();
        }

        private void btntimkiem_Click(object sender, EventArgs e)
        {
            TimKiem f = new TimKiem();
            f.ShowDialog();
        }
        int dong;
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dong = dataGridView1.CurrentRow.Index;
            txtmasach.Text = dataGridView1.Rows[dong].Cells[0].Value.ToString();
            txttensach.Text = dataGridView1.Rows[dong].Cells[1].Value.ToString();
            txtmaloai.Text = dataGridView1.Rows[dong].Cells[4].Value.ToString();
            txtsoluong.Text = dataGridView1.Rows[dong].Cells[2].Value.ToString();
            txtmatg.Text = dataGridView1.Rows[dong].Cells[3].Value.ToString();
        }

        private void btnquanlytacgia_Click(object sender, EventArgs e)
        {
            QuanLyTacGia f = new QuanLyTacGia();
            f.ShowDialog();
        }

        private void btnquanlymuontra_Click(object sender, EventArgs e)
        {
            QuanLyMuonTra f = new QuanLyMuonTra();
            f.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
