﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DAL;

namespace GUI
{
    public partial class QuanLyTacGia : Form
    {
        public QuanLyTacGia()
        {
            InitializeComponent();
        }
        D_QuanLyTacGia d_qltg = new D_QuanLyTacGia();
        B_QuanLyTacGia b_qltg = new B_QuanLyTacGia();
        DanhMuc dm = new DanhMuc();
        Connect_Data cd = new Connect_Data();
        private void QuanLyTacGia_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dm.HienThiTacGia();
           
        }
        int dong;
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dong = dataGridView1.CurrentRow.Index;
            txtMaTG.Text = dataGridView1.Rows[dong].Cells[0].Value.ToString();
            txtTenTG.Text = dataGridView1.Rows[dong].Cells[1].Value.ToString();
            txtDiaChi.Text = dataGridView1.Rows[dong].Cells[2].Value.ToString();
          
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void QuanLyTacGia_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show("Dữ liệu bạn nhập sẽ không được lưu, bạn có chắc chắn muốn đóng cửa sổ này?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dl == DialogResult.No)
                e.Cancel = true;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (txtMaTG.TextLength == 0)
                MessageBox.Show("Bạn cần chọn mã tác giả để xóa");
            else
                d_qltg.Matg = txtMaTG.Text;
            b_qltg.xoa(d_qltg.Matg);
            QuanLyTacGia_Load(sender, e);
        }

        private void btnnhaplai_Click(object sender, EventArgs e)
        {
            txtTenTG.Text = "";
            txtMaTG.Text = "";
            txtDiaChi.Text = "";
            txtMaTG.Focus();
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            if (txtMaTG.TextLength == 0)
                MessageBox.Show("Mã tác giả không được bỏ trống");
            else if (txtTenTG.TextLength == 0)
                MessageBox.Show("Tên tác giả không được bỏ trống");
            else if (txtDiaChi.TextLength == 0)
                MessageBox.Show("Địa chỉ không được bỏ trống");
            else
            {
                try
                {
                    d_qltg.Matg = txtMaTG.Text;
                    d_qltg.Tentg = txtTenTG.Text;
                    d_qltg.Diachi = txtDiaChi.Text;
                    b_qltg.them(d_qltg.Matg,d_qltg.Tentg,d_qltg.Diachi);
                    MessageBox.Show("Đã thêm thành công");
                    QuanLyTacGia_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            if (txtMaTG.TextLength == 0)
                MessageBox.Show("Mã tác giả không được bỏ trống");
            else if (txtTenTG.TextLength == 0)
                MessageBox.Show("Tên tác giả không được bỏ trống");
            else if (txtDiaChi.TextLength == 0)
                MessageBox.Show("Địa chỉ không được bỏ trống");
            else
            {
                try
                {
                    d_qltg.Matg = txtMaTG.Text;
                    d_qltg.Tentg = txtTenTG.Text;
                    d_qltg.Diachi = txtDiaChi.Text;
                    b_qltg.sua(d_qltg.Matg, d_qltg.Tentg, d_qltg.Diachi);
                    MessageBox.Show("Đã sửa thành công");
                    QuanLyTacGia_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }
    }
}
