﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;

namespace GUI
{
    public partial class TimKiem : Form
    {
        B_TimKiem b_tk = new B_TimKiem();
        public TimKiem()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if (btnTimKiem.Text == "Tìm Kiếm")
            {
                if (txtTimSach.TextLength == 0)
                    MessageBox.Show("Bạn chưa nhập từ khóa tìm kiếm ");
                else
                {
                    if (radMasach.Checked == false && radTensach.Checked == false)
                    {
                        MessageBox.Show("Vui lòng lựa chọn tìm kiếm theo mã sách hoặc tên sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        btnTimKiem.Text = "Thử lại";
                        txtTimSach.Enabled = false;
                        if (radMasach.Checked == true)
                        {
                            dataGridView1.DataSource = b_tk.TimKiemTheoMS(txtTimSach.Text);
                        }
                        else if (radTensach.Checked == true)
                        {
                            dataGridView1.DataSource = b_tk.TimKiemTheoTS(txtTimSach.Text);
                        }
                        else
                        {
                            
                            txt.Text = "Không tìm thấy sách này trong thông tin sách";
                        }
                        
                    }
                }
            }
            else
            {
                {
                    btnTimKiem.Text = "Tìm Kiếm";
                    txtTimSach.Enabled = true;
                    txtTimSach.Text = "";
                    txtTimSach.Focus();
                }
            }
        }

        private void TimKiem_Load(object sender, EventArgs e)
        {

        }
    }
}
