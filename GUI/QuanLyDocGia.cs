﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using BUS;

namespace GUI
{
    public partial class QuanLyDocGia : Form
    {
        D_QuanLyDocGia d_ql = new D_QuanLyDocGia();
        B_QuanLyDocGia b_ql = new B_QuanLyDocGia();
        DanhMuc dm = new DanhMuc();
        Connect_Data cd = new Connect_Data();
        public QuanLyDocGia()
        {
            InitializeComponent();
        }

        private void QuanLyDocGia_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dm.HienThiDocGia();
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void QuanLyDocGia_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show("Dữ liệu bạn nhập sẽ không được lưu, bạn có chắc chắn muốn đóng cửa sổ này?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dl == DialogResult.No)
                e.Cancel = true;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (txtmadocgia.TextLength == 0)
                MessageBox.Show("Bạn cần chọn mã độc giả để xóa");
            else
                d_ql.Madg = txtmadocgia.Text;
            b_ql.xoa(d_ql.Madg);
            QuanLyDocGia_Load(sender, e);
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            if (txtmadocgia.TextLength == 0)
                MessageBox.Show("Mã lớp không được bỏ trống");
            else if (txttendocgia.TextLength == 0)
                MessageBox.Show("Tên lớp không được bỏ trống");
            else if (txtgioitinh.TextLength == 0)
                MessageBox.Show("Mã loại không được bỏ trống");
            else if (txtngaymuon.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtdiachi.TextLength == 0)
                MessageBox.Show("Tác giả không được bỏ trống");
            else
            {
                try
                {
                    d_ql.Madg = txtmadocgia.Text;
                    d_ql.Tendg = txttendocgia.Text;
                    d_ql.Gioitinh = txtgioitinh.Text;
                    d_ql.Ngaymuon = txtngaymuon.Text;
                    d_ql.Diachi = txtdiachi.Text;
                    b_ql.them(d_ql.Madg,d_ql.Tendg,d_ql.Gioitinh,d_ql.Ngaymuon,d_ql.Diachi);
                    MessageBox.Show("Đã thêm thành công");
                    QuanLyDocGia_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        int dong;
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dong = dataGridView1.CurrentRow.Index;
            txtmadocgia.Text = dataGridView1.Rows[dong].Cells[0].Value.ToString();
            txttendocgia.Text = dataGridView1.Rows[dong].Cells[1].Value.ToString();
            txtgioitinh.Text = dataGridView1.Rows[dong].Cells[2].Value.ToString();
            txtngaymuon.Text = dataGridView1.Rows[dong].Cells[3].Value.ToString();
            txtdiachi.Text = dataGridView1.Rows[dong].Cells[4].Value.ToString();
        }

        private void btnnhaplai_Click(object sender, EventArgs e)
        {
            txttendocgia.Text = "";
            txtmadocgia.Text = "";
            txtgioitinh.Text = "";
            txtngaymuon.Text = "";
            txtdiachi.Text = "";
            txtmadocgia.Focus();
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            if (txtmadocgia.TextLength == 0)
                MessageBox.Show("Mã lớp không được bỏ trống");
            else if (txttendocgia.TextLength == 0)
                MessageBox.Show("Tên lớp không được bỏ trống");
            else if (txtgioitinh.TextLength == 0)
                MessageBox.Show("Mã loại không được bỏ trống");
            else if (txtngaymuon.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtdiachi.TextLength == 0)
                MessageBox.Show("Tác giả không được bỏ trống");
            else
            {
                try
                {
                    d_ql.Madg = txtmadocgia.Text;
                    d_ql.Tendg = txttendocgia.Text;
                    d_ql.Gioitinh = txtgioitinh.Text;
                    d_ql.Ngaymuon = txtngaymuon.Text;
                    d_ql.Diachi = txtdiachi.Text;
                    b_ql.sua(d_ql.Madg, d_ql.Tendg, d_ql.Gioitinh, d_ql.Ngaymuon, d_ql.Diachi);
                    MessageBox.Show("Đã sửa thành công");
                    QuanLyDocGia_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }
    }
}
