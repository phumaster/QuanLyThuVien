﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DAL;

namespace GUI
{
    public partial class QuanLyMuonTra : Form
    {
        public QuanLyMuonTra()
        {
            InitializeComponent();
        }
        DanhMuc dm=new DanhMuc();
        D_QuanLyMuonTra d_qlmt = new D_QuanLyMuonTra();
        B_QuanLyMuonTra b_qlmt = new B_QuanLyMuonTra();
        private void btnChoMuon_Click(object sender, EventArgs e)
        {
            if (txtMaDG.TextLength == 0)
                MessageBox.Show("Mã độc giả không được bỏ trống");
            else if (txtMaSach.TextLength == 0)
                MessageBox.Show("Mã sách không được bỏ trống");
            else if (txtSoLuong.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtNgayMuon.TextLength == 0)
                MessageBox.Show("Ngày mượn không được bỏ trống");
            else if (txtNgayHenTra.TextLength == 0)
                MessageBox.Show("Ngày hẹn trả không được bỏ trống");
            else
            {
                try
                {
                    d_qlmt.Madg = txtMaDG.Text;
                    d_qlmt.Masach = txtMaSach.Text;
                    d_qlmt.Soluong = txtSoLuong.Text;
                    d_qlmt.Ngaymuon = txtNgayMuon.Text;
                    d_qlmt.Ngayhentra = txtNgayHenTra.Text;
                    d_qlmt.Ngaytra = "";
                    b_qlmt.muon(d_qlmt.Madg, d_qlmt.Masach, d_qlmt.Soluong, d_qlmt.Ngaymuon, d_qlmt.Ngayhentra,d_qlmt.Ngaytra);
                    MessageBox.Show("Đã cho mượn");
                    QuanLyMuonTra_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }

        private void btnMuonMoi_Click(object sender, EventArgs e)
        {
            txtMaDG.Text = "";
            txtMaSach.Text = "";
            txtSoLuong.Text = "";
            txtNgayMuon.Text = "";
            txtNgayHenTra.Text = "";
            txtMaDG.Focus();
        }

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void QuanLyMuonTra_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show("Dữ liệu bạn nhập sẽ không được lưu, bạn có chắc chắn muốn đóng cửa sổ này?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dl == DialogResult.No)
                e.Cancel = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabQLMuon_Click(object sender, EventArgs e)
        {

        }

        private void QuanLyMuonTra_Load(object sender, EventArgs e)
        {
               dataGridView1.DataSource = dm.HienThiMuonSach();
               dataGridView2.DataSource = dm.HienThiTraSach();
        }
        int dong;
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dong = dataGridView1.CurrentRow.Index;
            txtMaDG.Text = dataGridView1.Rows[dong].Cells[0].Value.ToString();
            txtMaSach.Text = dataGridView1.Rows[dong].Cells[1].Value.ToString();
            txtSoLuong.Text = dataGridView1.Rows[dong].Cells[2].Value.ToString();
            txtNgayMuon.Text = dataGridView1.Rows[dong].Cells[3].Value.ToString();
            txtNgayHenTra.Text = dataGridView1.Rows[dong].Cells[4].Value.ToString();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtmadg1.TextLength == 0)
                MessageBox.Show("Mã độc giả không được bỏ trống");
            else if (txtmasach1.TextLength == 0)
                MessageBox.Show("Mã sách không được bỏ trống");
            else if (txtsoluong1.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtngaymuon1.TextLength == 0)
                MessageBox.Show("Ngày mượn không được bỏ trống");
            else if (txtngayhentra1.TextLength == 0)
                MessageBox.Show("Ngày hẹn trả không được bỏ trống");
            else if (txtngaytra1.TextLength == 0)
                MessageBox.Show("Ngày trả không được bỏ trống");
            else
            {
                try
                {
                    d_qlmt.Madg = txtmadg1.Text;
                    d_qlmt.Masach = txtmasach1.Text;
                    d_qlmt.Soluong = txtsoluong1.Text;
                    d_qlmt.Ngaymuon = txtngaymuon1.Text;
                    d_qlmt.Ngayhentra = txtngayhentra1.Text;
                    d_qlmt.Ngaytra = txtngaytra1.Text;
                    b_qlmt.tra(d_qlmt.Madg, d_qlmt.Masach, d_qlmt.Soluong, d_qlmt.Ngaymuon, d_qlmt.Ngayhentra, d_qlmt.Ngaytra);
                    MessageBox.Show("Đã trả");
                    label19.Text = "Đã Trả Sách ";
                    QuanLyMuonTra_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }

        private void dataGridView2_Click(object sender, EventArgs e)
        {
            dong = dataGridView2.CurrentRow.Index;
            txtmadg1.Text = dataGridView2.Rows[dong].Cells[0].Value.ToString();
            txtmasach1.Text = dataGridView2.Rows[dong].Cells[1].Value.ToString();
            txtsoluong1.Text = dataGridView2.Rows[dong].Cells[2].Value.ToString();
            txtngaymuon1.Text = dataGridView2.Rows[dong].Cells[3].Value.ToString();
            txtngayhentra1.Text = dataGridView2.Rows[dong].Cells[4].Value.ToString();
            txtngaytra1.Text = dataGridView2.Rows[dong].Cells[5].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtmadg1.Text = "";
            txtmasach1.Text = "";
            txtsoluong1.Text = "";
            txtngaymuon1.Text = "";
            txtngayhentra1.Text = "";
            txtngaytra1.Text = "";
            txtMaDG.Focus();
        }

   

       
    }
}
