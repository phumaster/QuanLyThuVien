﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using BUS;

namespace GUI
{
    public partial class CapNhat : Form
    {
        D_CapNhat d_cn = new D_CapNhat();
        B_CapNhat b_cn = new B_CapNhat();
        DanhMuc dm = new DanhMuc();
        Connect_Data cd = new Connect_Data();
        private void CapNhat_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dm.HienThi();
        }
        public CapNhat()
        {
            InitializeComponent();
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CapNhat_FormClosing(object sender, FormClosingEventArgs e)
        {
            TrangChu trangchu = (TrangChu)Application.OpenForms["TrangChu"];
            trangchu.loadData();
            DialogResult dl = MessageBox.Show("Dữ liệu bạn nhập sẽ không được lưu, bạn có chắc chắn muốn đóng cửa sổ này?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dl == DialogResult.No)
                e.Cancel = true;
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            if (txtmasach.TextLength == 0)
                MessageBox.Show("Mã lớp không được bỏ trống");
            else if(txttensach.TextLength==0)
                MessageBox.Show("Tên lớp không được bỏ trống");
            else if (txtmaloai.TextLength == 0)
                MessageBox.Show("Mã loại không được bỏ trống");
            else if (txtsoluong.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtmatg.TextLength == 0)
                MessageBox.Show("Tác giả không được bỏ trống");
            else
            {
                try
                {
                    d_cn.Masach_ = txtmasach.Text;
                    d_cn.Tensach_ = txttensach.Text;
                    d_cn.Maloai_ = txtmaloai.Text;
                    d_cn.Soluong = Int32.Parse(txtsoluong.Text);
                    d_cn.Matacgia_ = txtmatg.Text;
                    b_cn.them(d_cn.Masach_, d_cn.Tensach_, d_cn.Soluong, d_cn.Maloai_, d_cn.Matacgia_);
                    MessageBox.Show("Đã thêm thành công");
                    CapNhat_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
               
            }
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (txtmasach.TextLength == 0)
                MessageBox.Show("Bạn cần chọn mã sách để xóa");
            else
                d_cn.Masach_ = txtmasach.Text;
            b_cn.xoa(d_cn.Masach_);
            CapNhat_Load(sender, e);
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            if (txtmasach.TextLength == 0)
                MessageBox.Show("Mã lớp không được bỏ trống");
            else if (txttensach.TextLength == 0)
                MessageBox.Show("Tên lớp không được bỏ trống");
            else if (txtmaloai.TextLength == 0)
                MessageBox.Show("Mã loại không được bỏ trống");
            else if (txtsoluong.TextLength == 0)
                MessageBox.Show("Số lượng không được bỏ trống");
            else if (txtmatg.TextLength == 0)
                MessageBox.Show("Tác giả không được bỏ trống");
            else
            {
                try
                {
                    d_cn.Masach_ = txtmasach.Text;
                    d_cn.Tensach_ = txttensach.Text;
                    d_cn.Soluong = Int32.Parse(txtsoluong.Text);
                    d_cn.Matacgia_ = txtmatg.Text;
                    d_cn.Maloai_ = txtmaloai.Text;
                    b_cn.sua(d_cn.Masach_, d_cn.Tensach_, d_cn.Soluong, d_cn.Maloai_, d_cn.Matacgia_);
                    MessageBox.Show("Đã sửa");
                    CapNhat_Load(sender, e);
                }
                catch { MessageBox.Show("Có lỗi!!"); }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
             
        }
        int dong;
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dong = dataGridView1.CurrentRow.Index;
            txtmasach.Text = dataGridView1.Rows[dong].Cells[0].Value.ToString();
            txttensach.Text = dataGridView1.Rows[dong].Cells[1].Value.ToString();
            txtmaloai.Text = dataGridView1.Rows[dong].Cells[4].Value.ToString();
            txtsoluong.Text = dataGridView1.Rows[dong].Cells[2].Value.ToString();
            txtmatg.Text = dataGridView1.Rows[dong].Cells[3].Value.ToString();
        }

        private void btnnhaplai_Click(object sender, EventArgs e)
        {
            txttensach.Text = "";
            txtmasach.Text = "";
            txtsoluong.Text = "";
            txtmaloai.Text = "";
            txtmatg.Text = "";
            txtmasach.Focus();
        }

        private void txttacgia_TextChanged(object sender, EventArgs e)
        {

        }
        
    }
        

      

      

       
    }

